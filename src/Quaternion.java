import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Double.parseDouble;
import static java.lang.String.format;

/** Quaternions. Basic operations. */
   public class Quaternion {

      private final double E = 0.00000000000001;

      private final double real;
      private final double i;
      private final double j;
      private final double k;
// test
   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
      this.real = a;
      this.i = b;
      this.j = c;
      this.k = d;
   }

   private boolean isCloseToZero(double d) {
      return Math.abs(d) <= E;
      }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return real;
   }

   /** Imaginary part i of the quaternion. 
    * @return imaginary part i
    */
   public double getIpart() {
      return i;
   }

   /** Imaginary part j of the quaternion. 
    * @return imaginary part j
    */
   public double getJpart() {
      return j;
   }

   /** Imaginary part k of the quaternion. 
    * @return imaginary part k
    */
   public double getKpart() {
      return k;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion: 
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
      return format("%f%s%fi%s%fj%s%fk", real, getSign(i), i, getSign(j), j, getSign(k), k);
   }

   private String getSign(double i) {
      return i < 0 ? "" : "+";
   }

   /** Conversion from the string to the quaternion. 
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent 
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) {
      String regexDouble = "[0-9]+(\\.[0-9]+)?";
      String regex = format("(-?%s)([\\+-]%s)i([\\+-]%s)j([\\+-]%s)k", regexDouble, regexDouble, regexDouble, regexDouble);
      Matcher matcher = Pattern.compile(regex).matcher(s);

      try {
      if(!matcher.matches())
         throw new IllegalArgumentException("wrong format");

      return new Quaternion(
              tryParseDouble(matcher.group(1), "real"),
              tryParseDouble(matcher.group(3), "i"),
              tryParseDouble(matcher.group(5), "j"),
              tryParseDouble(matcher.group(7), "k"));
   }catch(Exception e) {
         throw new RuntimeException(format("Input: %s, Error: %s", s, e.getMessage()));
      }
   }
   private static double tryParseDouble(String s, String descr) {
      try {
         s = s.replaceAll(",", ".");
         return parseDouble(s);
      } catch(Exception e) {
         throw new RuntimeException(format("cannot parse '%s' part: %s - %s", descr, s, e.getMessage()));
      }
   }
   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Quaternion(real, i, j, k);
   }

   /** Test whether the quaternion is zero. 
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
      return isCloseToZero(real) && isCloseToZero(i) && isCloseToZero(j) && isCloseToZero(k);
   }
   /** Conjugate of the quaternion. Expressed by the formula 
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
      return new Quaternion(real, -i, -j, -k);
   }
   /** Opposite of the quaternion. Expressed by the formula 
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
      return new Quaternion(-real, -i, -j, -k);
   }
   /** Sum of quaternions. Expressed by the formula 
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
      return new Quaternion(this.getRpart() + q.getRpart(),
              this.getIpart() + q.getIpart(),
              this.getJpart() + q.getJpart(),
              this.getKpart() + q.getKpart());
   }
   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
      double r = this.getRpart();
      double i = this.getIpart();
      double j = this.getJpart();
      double k = this.getKpart();

      double r1 = q.getRpart();
      double i1 = q.getIpart();
      double j1 = q.getJpart();
      double k1 = q.getKpart();

      return new Quaternion(
              r * r1 - i * i1 - j * j1 - k * k1,
              r * i1 + i * r1 + j * k1 - k * j1,
              r * j1 - i * k1 + j * r1 + k * i1,
              r * k1 + i * j1 - j * i1 + k * r1);
   }
   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
      return new Quaternion(real * r, i * r, j * r, k * r);
   }
   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) + 
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
      public Quaternion inverse() {
      if(isZero())
         throw new RuntimeException("cannot inverse zero");
      double square = real * real + i * i + j * j + k * k;
      return new Quaternion(real / square, -i / square, -j / square, -k / square);
   }
   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
      return new Quaternion(real - q.getRpart(), i - q.getIpart(), j - q.getJpart(), k - q.getKpart());
   }
   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
      if(q.isZero())
         throw new RuntimeException("cannot divide by zero");
      return times(q.inverse());
   }
   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
      if(isZero())
         throw new RuntimeException("cannot divide by zero");
      return q.inverse().times(this);
   }
   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
      if(qo == null)
         return false;
      if(this == qo) {
         return true;
      }
      if (!(qo instanceof Quaternion))
         return false;
      final Quaternion q = (Quaternion) qo;

      return isCloseToZero(real - q.getRpart())
              && isCloseToZero(i - q.getIpart())
              && isCloseToZero(j - q.getJpart())
              && isCloseToZero(k - q.getKpart());
   }
   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
      return times(q.conjugate()).plus(q.times(conjugate())).times(0.5);
   }
   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Arrays.hashCode(new double[] { real, i, j, k });
   }

   /** Norm of the quaternion. Expressed by the formula 
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(real * real + i * i + j * j + k * k);
   }
   /** Main method for testing purposes. 
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
      Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: "
              + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
      System.out.println ("dotMult: " + arv1.dotMult (arv2));
   }
}
// end of file
